#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "html.h"

int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
  char *src = malloc(size + 1);
  memcpy(src, data, size);
  src[size] = 0;

  char *dst = html_escape(src);

  free(src);
  free(dst);

  return 0;
}
