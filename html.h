#ifndef IDATT2503_HTML_H
#define IDATT2503_HTML_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char *html_escape(const char *src)
{
    if (!src) 
        return NULL;

    size_t length = 0;
    char *srcPtr = (char *) src;

    while (*srcPtr)
    {
        switch (*srcPtr++)
        {
            case '&': length += 5; break;
            case '<': length += 4; break;
            case '>': length += 4; break;
            default:  length++;    break;
        }
    }

    char *dst = malloc(length + 1);
    if (!dst) 
        return NULL;
    
    dst[length] = 0;
    
    char *dstPtr = dst;
    srcPtr = (char *) src;

    while (*srcPtr) 
    {
        switch (*srcPtr)
        {
        case '&':
            memcpy(dstPtr, "&amp;", 5); 
            dstPtr += 5; 
            break;
        case '<': 
            memcpy(dstPtr, "&lt;", 4); 
            dstPtr += 4;
            break;
        case '>': 
            memcpy(dstPtr, "&gt;", 4); 
            dstPtr += 4; 
            break;
        default:  
            *dstPtr = *srcPtr;
            dstPtr++; 
            break;
        }
        srcPtr++;
    }

    return dst;
}

#endif
