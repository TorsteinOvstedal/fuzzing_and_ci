#include <stdlib.h>
#include <stdio.h>
#include "html.h"

int main(void)
{
    const char *str = 
        "<script>\n"
        "  const c = a && b;\n"
        "</script>";

    char *result = html_escape(str);
    if (!result)
        return 1;
    
    printf("Original:\n%s\n\nEncoded:\n%s\n", str, result);
    
    free(result);

    return 0;   
}